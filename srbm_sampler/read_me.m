%-------------------------------------------------------------------------
%
% Step-by-step instructions on how to sample a binary pairwise Markov 
% network using the function srbm_sampler().
%
%-------------------------------------------------------------------------

%% Step 1 
% Create adjacency matrix of a 100-by-100 grid network (=> 10,000 nodes).

clear
nrows = 100;
ncols = 100;
d = nrows*ncols;
amat = create_grid_network(nrows,ncols); % <- function included.

%% Step 2
% Generate a d-by-d coupling matrix 'A' (sparse format) and d-by-1 bias 
% parameter vector 'b'.

% Sample coupling parameters from Uniform(-2,2). 
[i,j] = ind2sub(size(amat),find(triu(amat)));
temp = (rand(length(i),1)-0.5)*4;
A = sparse([i;j],[j;i],[temp;temp],d,d);

% Sample bias parameters from Uniform(-1,1).
b = 2*(rand(d,1)-0.5);

%% Step 3
% Generate 1000 observations from the specified model using a burn-in of 
% 5000 iterations and a thinning of 100 iterations.

% Specify the input parameters.
n_obs = 1000;
n_burnin = 5000;
n_thinning = 100;

% Run the sampler, should take around 1 minute on a standard computer.
tic
data = srbm_sampler(A,b,n_obs,n_burnin,n_thinning); 
toc 
