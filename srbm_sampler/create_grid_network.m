%-------------------------------------------------------------------------
% Creates a nrows-by-ncols four-neighbour grid graph (aka lattice graph).
%
%   INPUT:  nrows, number of rows.
%           ncols, number of columns.
%
%   OUTPUT: amat, d-by-d adjacency matrix in sparse format, where 
%                 d = nrows*ncols.
%
% Copyright (C) 2018 Johan Pensar
%-------------------------------------------------------------------------

function amat = create_grid_network(nrows,ncols)

    if min(nrows,ncols) < 2
        error('The minimum number of rows/columns is two.')
    end    
    d = nrows*ncols;
    k1 = max(nrows-2,0);
    k2 = max(ncols-2,0);
    n_edges = (8+(k1+k2)*6+k1*k2*4)/2;    
    edges = zeros(n_edges,2);
    ind = 1;
    for i = 2:ncols
        edges(ind,:) = [i i-1];
        ind = ind+1;
    end
    for i = 1:(nrows-1)
        for pos = (i*ncols+1):((i+1)*ncols) 
            if pos == (i*ncols+1)
                edges(ind,:) = [pos pos-ncols];
                ind = ind+1;
            else
                edges(ind,:) = [pos pos-ncols];
                edges(ind+1,:) = [pos,pos-1];
                ind = ind+2;
            end
        end
    end
    amat = sparse([edges(:,1);edges(:,2)],[edges(:,2);edges(:,1)],1,d,d);
    
end  