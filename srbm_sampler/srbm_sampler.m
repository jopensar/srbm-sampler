%-------------------------------------------------------------------------
% An sRBM based Gibbs sampler for generating observations from a pairwise
% Markov network over d binary variables (with outcomes 0,1). The
% parameters are defined such that outcome 0 is the baseline category. For
% more details, see
%
% Pensar et al (under review) "High-dimensional structure learning of 
% binary pairwise Markov networks: A comparative numerical study.
%
%   INPUT:  A, d-by-d symmetric coupling matrix.
%           b, d-by-1 bias parameter vector.
%           n_obs, number of observations to generate.
%           n_burnin, number of burn-in iterations.
%           n_thinning, number of thinning iterations between samples.
%
%   OUTPUT: data, n_obs-by-d data matrix.
%
% Copyright (C) 2018 Johan Pensar
%-------------------------------------------------------------------------

function data = srbm_sampler(A,b,n_obs,n_burnin,n_thinning)    
    
    d = length(b); 
    
    % Pick out the edge potentials from coupling matrix. 
    ind = find(triu(A));
    [i,j] = ind2sub([d d],ind);
    edges = [i j];
    n_edges = size(edges,1);
    cval = A(ind);
    
    % Construct the W matrix containing the coupling parameters between the
    % binary variables X and the Gaussian auxiliary variables Y.
    W = sparse( [1:n_edges 1:n_edges]',...
                [edges(:,1);edges(:,2)],...
                [sqrt(abs(cval));sqrt(abs(cval)).*sign(cval)]);
    W(1,d) = 0;

    % Adjust b to get the correct node potentials.   
    b = b-0.5*diag(W'*W);

    % Run the burn-in chain.
    x = rand(d,1) < 0.5; % Random initial configuration.
    for i = 1:n_burnin
        y = W*x+randn(n_edges,1); % Sample y given x.        
        m = exp(W'*y+b);
        x = (m./(m+1)) > rand(d,1); % Sample x given y.
    end

    % Run the actual sampling chain.
    data = zeros(n_obs,d);
    pos = 1;
    skip = 0;
    while pos <= n_obs
        y = W*x+randn(n_edges,1); % Sample y given x.        
        m = exp(W'*y+b);
        x = (m./(m+1)) > rand(d,1); % Sample x given y.
        if skip == n_thinning % Thinning.
            data(pos,:) = x';
            pos = pos+1;
            skip = 0;
        else
            skip = skip+1;
        end
    end   
    
end